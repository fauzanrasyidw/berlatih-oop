<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->Name . "<br>";
    echo "legs : " . $sheep->Legs . "<br>";
    echo "Cold blooded : " . $sheep->Cold_blooded . "<br><br>";

    $katak = new Kodoq('buduk');
    echo "Name : " . $katak->Name . "<br>";
    echo "legs : " . $katak->Legs . "<br>";
    echo "Cold blooded : " . $katak->Cold_blooded . "<br>";
    echo "Jump : " . $katak->Lompat() . "<br><br>";

    $sungoku = new Ape('kera sakti');
    echo "Name : " . $sungoku->Name . "<br>";
    echo "legs : " . $sungoku->Legs . "<br>";
    echo "Cold blooded : " . $sungoku->Cold_blooded . "<br>";
    echo "Yell : " . $sungoku->Teriak() . "<br><br>";


?>